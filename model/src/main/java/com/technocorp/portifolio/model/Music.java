package com.technocorp.portifolio.model;

import lombok.*;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Music extends Media {
    @NonNull
    private String album;
}
